import { Component, OnInit } from '@angular/core';
import { Item } from '../models/item';
import { ItemsRepositoryService } from '../items-repository.service';
import { Observable } from 'rxjs';
import { strictEqual } from 'assert';

const ascendingString = "rosnąco";
const descendingString = "malejąco";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public numberOfPages: number;
  public Items: Item[];
  public priceSortOrderString: string = ascendingString;
  public titleSortOrderString: string = ascendingString;

  constructor(
    private readonly itemsRepository: ItemsRepositoryService
  ) { }

  ngOnInit() {
    this.itemsRepository.GetPage().subscribe(res => this.Items = res);
    this.itemsRepository.itemsCount.subscribe(count => {  
      if (count % 5 === 0) {
        this.numberOfPages = count / 5;
      } else {
        this.numberOfPages = (count / 5) + 1;
      }
     });
  }

  public getPage(pageNumber: number): void {
    const skip = (pageNumber - 1) * 5;

    this.itemsRepository.GetPage(skip).subscribe(res => this.Items = res);
  }

  public changePage(event: any): void {
    const pageNumber = parseInt(event.srcElement.innerHTML);
    this.getPage(pageNumber);
  }

  public sortByPrice(): void {
    if (this.priceSortOrderString === ascendingString) {
      this.Items.sort(this.sortAscendingByPrice);
      this.priceSortOrderString = descendingString;
    } else {
      this.Items.sort(this.sortDescendingByPrice);
      this.priceSortOrderString = ascendingString
    }
  }

  public sortByTitle(): void {
    if (this.titleSortOrderString === ascendingString) {
      this.Items.sort(this.sortAscendingByTitle);
      this.titleSortOrderString = descendingString;
    } else {
      this.Items.sort(this.sortDescendingByTitle);
      this.titleSortOrderString = ascendingString
    }
  }

  private sortAscendingByPrice(item1: Item, item2: Item): number {
    return item1.Price - item2.Price;
  }

  private sortDescendingByPrice(item1: Item, item2: Item): number {
    return item2.Price - item1.Price;
  }

  private sortAscendingByTitle(item1: Item, item2: Item): number {
    return item1.Title.toLowerCase().localeCompare(item2.Title.toLowerCase(), undefined, {numeric: true, sensitivity: 'base' });
  }

  private sortDescendingByTitle(item1: Item, item2: Item): number {
    return item2.Title.toLowerCase().localeCompare(item1.Title.toLowerCase(), undefined, {numeric: true, sensitivity: 'base' });
  }
}
