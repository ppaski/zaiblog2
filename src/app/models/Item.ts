
export interface Item
{
    Id: number;
    Title: string;
    Description: string;
    Author: string;
    PublishedOn: Date;
    Category: Category;
    ImageUrl: string
    Price: number;
    Tags: string[];
}

export enum Category{
    Book,
    Movie,
    Music
}