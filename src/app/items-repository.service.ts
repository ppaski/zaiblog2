import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Item } from './models/Item';
import { map } from 'rxjs/operators';
import { Observable, of, BehaviorSubject } from 'rxjs';

const key = "items";

@Injectable({
  providedIn: 'root'
})
export class ItemsRepositoryService implements OnInit {

  public itemsCount: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private guid: number;
  private items: Item[]
  constructor(
    private readonly httpClient: HttpClient
  ) { 
    this.items = [];
    const localStorageString = localStorage.getItem(key);
    if (localStorage.length > 0) {
      this.items = JSON.parse(localStorageString);
    }
    else {
      this.httpClient.get<Item[]>("assets/items.json").subscribe(
        result => {
          this.items = result;
          this.guid = this.items.length + 1;
        },
        error => console.log(error)
      );
      localStorage.setItem(key, JSON.stringify(this.items));
    }
  }

  ngOnInit(): void {
  }

  public GetPage(skip: number = 0): Observable<Item[]> {
    if (this.items.length === 0) {
      return this.httpClient.get<Item[]>("assets/items.json").pipe(
        map (
          result => {
            this.items = result;
            this.guid = this.items.length;
            this.itemsCount.next(this.items.length + 1);

            return this.items.slice(skip, 5);
          },
          error => console.log(error)
        )
      )
    }
    const itemsToReturn = this.items.slice(skip, skip + 5);

    return of(itemsToReturn);
  }

  public CreateItem(item: Item): void {
    this.guid = this.guid + 1;
    item.Id = this.guid;

    this.items.push(item)
    this.itemsCount.next(this.items.length);
    localStorage.setItem(key, JSON.stringify(this.items));
  }

  public EditItem(item: Item): void {
    let foundItem = this.items.find(i => i.Id === item.Id);

    foundItem.Author = item.Author;
    foundItem.Category = item.Category;
    foundItem.Description = item.Description;
    foundItem.ImageUrl = item.ImageUrl;
    foundItem.Price = item.Price;
    foundItem.PublishedOn = item.PublishedOn;
    foundItem.Tags = item.Tags;
    foundItem.Title = item.Title;

    localStorage.setItem(key, JSON.stringify(this.items));
  }

  public RemoveItem(itemId: number): void {
    const itemIndex = this.items.findIndex(i => i.Id === itemId);

    if (itemIndex < 0) {
      return;
    }

    this.items.splice(itemIndex, 1);
    this.itemsCount.next(this.items.length);
    localStorage.setItem(key, JSON.stringify(this.items));
  }
}
