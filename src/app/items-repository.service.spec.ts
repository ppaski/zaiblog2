import { TestBed } from '@angular/core/testing';

import { ItemsRepositoryService } from './items-repository.service';

describe('ItemsRepositoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ItemsRepositoryService = TestBed.get(ItemsRepositoryService);
    expect(service).toBeTruthy();
  });
});
