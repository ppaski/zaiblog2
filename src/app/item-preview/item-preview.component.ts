import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../models/Item';

@Component({
  selector: 'app-item-preview',
  templateUrl: './item-preview.component.html',
  styleUrls: ['./item-preview.component.css']
})
export class ItemPreviewComponent implements OnInit {

  @Input()
  Item: Item;
  
  constructor() { }

  ngOnInit() {
    console.log(this.Item.ImageUrl);
  }

}
