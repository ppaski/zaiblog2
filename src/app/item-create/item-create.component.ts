import { Component, OnInit } from '@angular/core';
import { Category, Item } from '../models/item';
import { ItemsRepositoryService } from '../items-repository.service';

@Component({
  selector: 'app-item-create',
  templateUrl: './item-create.component.html',
  styleUrls: ['./item-create.component.css']
})
export class ItemCreateComponent implements OnInit {
  public categories: Category[] = [
    Category.Book,
    Category.Movie,
    Category.Music
  ];
  public selectedCategory: Category = Category.Book;
  public title: string;
  public description: string;
  public author: string;
  public publishedOn: Date;
  public imageUrl: string;
  public price: number;
  public tags: string;

  public Category: typeof Category = Category;
  constructor(
    private readonly itemsRepository: ItemsRepositoryService
  ) { }

  ngOnInit() {
  }


  public create(): void {
    const item = {
      Title: this.title,
      Description: this.description,
      Author: this.author,
      PublishedOn: this.publishedOn,
      Category: this.selectedCategory,
      ImageUrl: this.imageUrl,
      Price: this.price,
      Tags: this.tags.split(',')
    } as Item;
    
    this.itemsRepository.CreateItem(item);
  }
}
